const goTo = (url) => {
    location.href = url;
}

const goToNew = (url) => {
    const win = window.open(url, '_blank');
    win.focus();
}